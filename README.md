#CPSC 4820 Final Project: 2D Racing Game AI#
##Team Members##
Hakeem Jones, David Kwietniewski, Casey Merritt
##Project Description##
This project is a 2D racing program with an artificial neural network back end.  Our program allows for the racing of 4 virtual robots around a square track.  The virtual robots make decisions based on their sensor inputs that are then input into an artificial neural network.  Each robot can move forward, left, right, or backwards based on the outcome of the artificial neural network.

rosNeuralNet.py allows for the training of the artificial neural network

Once ROS is started with roslaunch ros_web_service stage.launch track0.launch:
driveRobot.py allows for manual control of a virtual robot
rosRace.py allows for robots to race each other on Track0

To run:
Make sure paths are correct in track0.launch and track0.world
roslaunch ros_web_service stage.launch track0.launch
python rosRace.py