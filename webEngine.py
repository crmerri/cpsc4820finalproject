import urllib2
import time

def getData( url, delimeter ):
	sensorData = []
	try:
		data = urllib2.urlopen( url )
		rawSensorData = data.read()
		data.close()
		sensorData = [float(x) for x in rawSensorData.split(delimeter)]
	except:
		pass;
	return sensorData

def sendRequest( url ):
	try:
		data = urllib2.urlopen( url )
		data.close()
	except:
		pass;

def getPositionData( id = 0 ):
	return getData("http://127.0.0.1:8082/state?id=" + str(id), ',')

def getSensorData( id = 0 ):
	return getData("http://127.0.0.1:8083/twist?id=" + str(id), ' ')

def move(id=0,  direction = "forward", speed = 0.3 ):
	if( direction == "forward" ):
		sendRequest("http://127.0.0.1:8083/twist?id=" + str(id) +  "&lx=" + str(speed))
	elif( direction == "backward" ):
		sendRequest("http://127.0.0.1:8083/twist?id=" + str(id) +  "&lx=-" + str(speed))
	else:
		print("move in unknown direction")
	
def rotate( id = 0, direction = "left", speed = 0.3 ):
	if( direction == "left" ):
		data = sendRequest("http://127.0.0.1:8083/twist?id=" + str(id) +  "&az=" + str(speed))
	elif( direction == "right" ):
		data = sendRequest("http://127.0.0.1:8083/twist?id=" + str(id) +  "&az=-" + str(speed))
	else:
		print("rotate in unknown direction")
		
def turn( id = 0, direction = "left", speed = 0.3, duration = 1 ):
	if( direction == "left" ):
		data = sendRequest("http://127.0.0.1:8083/twist?id=" + str(id) +  "&az=" + str(speed))
	elif( direction == "right" ):
		data = sendRequest("http://127.0.0.1:8083/twist?id=" + str(id) +  "&az=-" + str(speed))
	else:
		print("turn in unknown direction")
	
	time.sleep(duration);
	stop( id )

def stop( id = 0 ):
	sendRequest("http://127.0.0.1:8083/twist?id=" + str(id) +  "&az=0&lx=0" )