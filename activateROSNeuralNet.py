import random
import threading

from numpy import *

from pybrain.tools.shortcuts import buildNetwork
from pybrain.datasets import SupervisedDataSet
from pybrain.supervised.trainers import BackpropTrainer
from pybrain.tools.customxml.networkreader import NetworkReader

def netOutputToDecision( netOutput ):
	value = netOutput[0] << 2 | netOutput[1] << 1 | netOutput[2]
	
	rValue = ["", ""]
	
	if value == 1:
		rValue[0] = "move"
		rValue[1] = "forward"
	elif value == 2:
		rValue[0] = "move"
		rValue[1] = "backward"
	elif value == 3:
		rValue[0] = "turn"
		rValue[1] = "left"
	elif value ==4:
		rValue[0] = "turn"
		rValue[1] = "right"
	elif value == 5:
		rValue[0] = "turn"
		rValue[1] = "right"
	elif value == 6:
		rValue[0] = "stop"
	else:
		rValue[0] = "stop"	
		
	return rValue

net = None

# open up a saved neural net object and make a move
readLock = threading.Lock()
def activate( sensorData ):
	global net
	if net == None:
		readLock.acquire()
		print "Loading neural net"
		net = NetworkReader.readFrom('neuralNet.xml')
		readLock.release()
		
	linearStepOutput = net.activate( sensorData )

	decisionArray = []
	for e in linearStepOutput:
		if e > 0.5 :
			decisionArray.append( 1 )
		else:
			decisionArray.append( 0 )

	return decisionArray