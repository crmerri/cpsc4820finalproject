from activateROSNeuralNet import *
from webEngine import *

import time
import threading
import random

def makeDecision( activateOutput, robotID ):
	decision = netOutputToDecision( activateOutput )
	if decision[0] == "move":
		move( robotID, decision[1], speed=speedValue )
	if decision[0] == "turn":
		rotate( robotID, decision[1], speed=speedValue )
	if decision[0] == "stop":
		stop( robotID )

def robotDecision( robotID, duration= 1.0 ):
	sensorData = getSensorData( robotID )
	activateOutput = activate(sensorData)
	makeDecision( activateOutput, robotID )
	time.sleep(duration)
	stop( robotID )

def decisionRound( robotCount ):
	threadArray = [ ]
	for i in range( 0, robotCount ):
		threadArray.append(  threading.Thread(target=robotDecision, args=(i,durationValue,)) ) 
		
	for rThread in threadArray:
		rThread.start()
		
	for rThread in threadArray:
		rThread.join()

def handleStuckBots( robotCount, robotPositionsArray ):
	newPositionArray = []
	for j in range( 0, robotCount ):
		newPositionArray.append( getPositionData( j ) ) 
		if cmp( robotPositionsArray[j], newPositionArray[j] ) == 0:
			# deal with race condition
			#if bool(random.getrandbits(1)):
			move( j, "backward", speed = speedValue )
			time.sleep(random.uniform(0.2, 1.5))
			stop( j )
	return newPositionArray

def handleStuckBot(i,robotPosition):
	newPosition = getPositionData(i)
	if cmp(robotPosition, newPosition) == 0:
		if bool(random.getrandbits(1)):
			move(i, "backward", speed = speedValue)
			time.sleep(random.uniform(0.2, 1.5))
			stop(i)

#def race( robotCount ):
#	robotPositionsArray = []
#	for i in range( 0, robotCount ):
#		robotPositionsArray.append( getPositionData( i ) ) 
		
#	for i in range(0, 1000):
#		decisionRound( robotCount )
#		robotPositionsArray = handleStuckBots( robotCount, robotPositionsArray )

def race( robotCount ):
	#setup checkpoints
	check1X = -10.0
	check1Y = 8.0
	check2X = -21.5
	check2Y = 0.0
	check3X = -10.0
	check3Y = -14.7

	#set up finish lines
	finishLinesX = 0.5
	finishLinesY = []
	robotPositionsArray = []
	finished = []
	started = []
	reachedCP1 = []
	reachedCP2 = []
	reachedCP3 = []
	for i in range(0, robotCount ):
		finishLinesY.append(getPositionData(i)[0] + 0.5)
		robotPositionsArray.append(getPositionData(i))
		finished.append(0)
		started.append(False)
		reachedCP1.append(False)
		reachedCP2.append(False)
		reachedCP3.append(False)

	print finishLinesX
	print finishLinesY
	print "size of finished: %d" % len(finished)
	print "size of started: %d" % len(started)
	print "size of reachedCP1 %d" % len(reachedCP1)
	print "size of reachedCP2 %d" % len(reachedCP2)
	print "size of reachedCP3 %d" % len(reachedCP3)
		
	place = 1
	for i in range(0, 1000):
		decisionRound(robotCount)
		newPositionArray = []

		for j in range(0, robotCount):
			newPositionArray.append(getPositionData(j)) 
			#newPositionArray[j] = handleStuckBot(j, newPositionArray[j] )
			
			#if cmp(robotPositionsArray[j], newPositionArray[j]) == 0:
			#	move(j, "backward", speed = speedValue)
			#	time.sleep(0.1)
			#	stop(j)
			#check if robot has finished the race
			#robot must be in correct area
			#robot has passed the finish line
			#all check points have been reached
			
			if newPositionArray[j][1] > finishLinesX - 5 and newPositionArray[j][1] < finishLinesX + 5:
				if robotPositionsArray[j][0] < finishLinesY[j] and newPositionArray[j][0] >= finishLinesY[j]:
					if started[j] and reachedCP1[j] and reachedCP2[j] and reachedCP3[j]:
						finished[j] = place
						place += 1
						print "robot %d finished" % j
					elif not started[j] and not reachedCP1[j] and not reachedCP2[j] and not reachedCP3[j]:
						started[j] = True
						print "robot %d started" % j
				elif robotPositionsArray[j][0] > finishLinesY[j] and newPositionArray[j][0] <= finishLinesY[j]:
					started[j] = False
					print "robot %d unstarted" % j

			#check if robot has reached checkpoint 3
			#robot must be in correct area
			#robot has passed check point 3
			#previous checkpoints have been reached
			if newPositionArray[j][0] > check3Y-5 and newPositionArray[j][0] < check3Y+5:
				if robotPositionsArray[j][1] < check3X and newPositionArray[j][1] >= check3X:
					if started[j] and reachedCP1[j] and reachedCP2[j]:
						reachedCP3[j] = True
						print "robot %d reached checkpoint 3" % j
				elif robotPositionsArray[j][1] > check3X and newPositionArray[j][1] <= check3X:
					reachedCP3[j] = False
					print "robot %d unreached checkpoint 3" % j

			#check if robot has reached checkpoint 2
			if newPositionArray[j][1] > check2X-5 and newPositionArray[j][1] < check2X+5:
				if robotPositionsArray[j][0] > check2Y and newPositionArray[j][0] <= check2Y:
					if started[j] and reachedCP1[j] and not reachedCP3[j]:
						reachedCP2[j] = True
						print "robot %d reached checkpoint 2" % j
				elif robotPositionsArray[j][0] < check2Y and newPositionArray[j][0] >= check2Y:
					reachedCP2[j] = False
					print "robot %d unreached checkpoint 2" % j
			
			#check if robot has reached checkpoint 1
			if newPositionArray[j][0] > check1Y-5 and newPositionArray[j][0] < check1Y+5:
				if robotPositionsArray[j][1] > check1X and newPositionArray[j][1] <= check1X:
					if started[j] and not reachedCP2[j] and not reachedCP3[j]:
						reachedCP1[j] = True
						print("robot %d reached checkpoint 1" % j)
				elif robotPositionsArray[j][1] < check1X and newPositionArray[j][1] >= check1X:
					reachedCP1[j] = False
					print "robot %d unreached checkpoint 1" % j

			#check if robot is stuck
			if cmp(robotPositionsArray[j], newPositionArray[j]) == 0:
					move(j, "backward", speed = speedValue)
					time.sleep(random.uniform(0.2, 1.5))
					stop(j)

			newPositionArray[j] = getPositionData(j)

			#robotPositionsArray = handleStuckBots(robotCount, newPositionArray )
			

			
		#robotPositionsArray = handleStuckBots(robotCount, newPositionArray )
		robotPositionsArray = newPositionArray

		finish = True

		for j in range(0, robotCount):
			if finished[j] == 0:
				finish = False

		if finish == True:
			break

	#print places
	for i in range(0, robotCount):
		print "robot %d finished as %d" % (i, finished[i])

durationValue = 0.2
speedValue = 0.5
race( 4 )	
