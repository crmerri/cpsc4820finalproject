from pybrain.tools.shortcuts import buildNetwork
from pybrain.datasets import SupervisedDataSet
from pybrain.supervised.trainers import BackpropTrainer

from pybrain.tools.shortcuts import buildNetwork

from pybrain.tools.customxml.networkwriter import NetworkWriter 
from pybrain.tools.customxml.networkreader import NetworkReader

from activateROSNeuralNet import activate

ds = SupervisedDataSet(20, 4 )

#startFromScratch = True;
startFromScratch = False;

if( startFromScratch ):
	net = buildNetwork( 20, 5, 4, bias=True )
else:
	net = NetworkReader.readFrom('neuralNet.xml')

inputArray = []
outputArray = []

# convert expected output to binary representation
def convertLetterToBinaryArray(  letter ):
	if letter == 'f':
		rValue = [0,0,1, 0]
	elif letter == 'b':
		rValue = [0,1,0, 0]
	elif letter == 'l':
		rValue = [0,1,1, 0]
	elif letter == 'r':
		rValue = [1,0,0 ,0]
	elif letter == 's':
		rValue = [1,0,1, 0]
	else:
		rValue = [0,0,0, 0]

	return rValue
			
# read inputs		
with open('trainingData.txt', 'r') as inf:
	for line in inf:
		i = 0
		try:
			sensorData = line.split('\t')
			inputV = eval(sensorData[0])
			outputV = sensorData[1]
			inputArray.append(inputV)
			# (f)orward	(b)ackward	(l)eft	(r)right
			outputArray.append(convertLetterToBinaryArray(outputV[0].lower()))
			i += 1
		except:
			print( "Error on line " + str(i) + " of trainingData.txt")
			exit()

# verify data
#for i in range(0, min(len(inputArray), 10)):
#	print(inputArray[i])
#	print ("\t" + str(outputArray[i]))
	
# add samples to neural net		
for i in range(0, len(inputArray)):
	ds.addSample(inputArray[i], outputArray[i])

# train neural net
trainer = BackpropTrainer(net, ds)
for i in range(0, 2):
	for i in range(0, 10000):
		trainer.train()
	trainer.trainUntilConvergence()
	# write neural net to file
	NetworkWriter.writeToFile(net, 'neuralNet.xml')

# check correctness
for i in range(1, len(inputArray)):
	decision = activate(inputArray[i])
	expected = outputArray[i]
	if( decision != expected ):
		print( str(i).ljust(4) + "| expected: " + str(expected) + " actual:  " + str(decision))