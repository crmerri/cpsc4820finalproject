from webEngine import *

# MUST HIT ENTER IN ORDER TO ISSUE COMMAND
#move forward	= w
#move backward	= s
#rotate left    	= a
#rotate right 	= d
#stop	 	= [space]
#print position	= p
#print sensor	= o
#quit		= q

def drive( id = 0 ):
	while( True ):
		key = raw_input()
		if( key == 'w' ):
			move(id,  "forward")
		elif( key == 's' ):
			move(id,  "backward")
		elif( key == 'a' ):
			rotate(id,  "left")
		elif( key == 'd' ):
			rotate(id, "right")
		elif( key == ' ' ):
			print(stop(id))
		elif( key == 'p' ):
			print(getPositionData(id))
		elif( key == 'o' ):
			print(getSensorData(id))
		elif( key == 'q' ):
			break

# change parameter to move different robot
drive( 0 )